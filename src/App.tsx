import React, { Component } from "react";
import logo from "./p-logo.jpg";
import Standings from "./components/Standings";
import "antd/dist/antd.css";
import "./App.css";
import DataService from "./services/dataService";

class App extends Component {
  state = {
    standings: DataService.calculateTeamPositionsPerRound()
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="Premier League" />
        </header>
        <Standings round={38} standings={this.state.standings} />
      </div>
    );
  }
}

export default App;
