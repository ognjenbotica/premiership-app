export interface ResultModel {
  Round: number;
  Teams: TeamModel[];
}

export interface TeamModel {
  Name: string;
  GameWon: number;
  GameDraw: number;
  GameLost: number;
  Points: number;
  GoalsDiff: number;
  GoalsScored: number;
  GoalsReceived: number;
  GamesPlayed: number;
  LastFiveGamesOutcome: GameOutcome[];
}

export interface GameOutcome {
  Key: number
  Value: string
}