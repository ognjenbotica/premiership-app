import React, { Component } from "react";
import { Select, Table, Tag } from "antd";
import "antd/dist/antd.css";
import { ResultModel, GameOutcome } from "../models/resultModel";
import _ from "lodash";

const { Option } = Select;

interface IResultProps{
  standings: ResultModel[]
  round: number
}

class Standings extends Component<IResultProps> {

  state = { 
    standings: this.props.standings, 
    roundNumber: this.props.round
  };

  handleChange: any = (value: number) => {

    this.setState({ roundNumber: value });
  }

  getRoundResult(round: number){
    return this.state.standings.filter((x) => x.Round === round)[0];
  }

  render() { return (
    <>
      <div className="ComponentContainer">
        <Select style={{ width: "100%" }}  defaultValue="Round 38" onChange={this.handleChange}>
          {getRoundsForDropdown(this.state.standings).map((d, i) => (
            <Option key={i} value={d.Round}>Round {d.Round}</Option>
          ))}
        </Select>
      </div>
      <div className="ComponentContainer">
        <Table
          dataSource={this.getRoundResult(this.state.roundNumber).Teams}
          columns={columns()}
          rowKey="Name"
          pagination={false}
          size="small"
        ></Table>
      </div>
    </>);
  }
}

function getRoundsForDropdown(standings: ResultModel[]) {
  return _.orderBy(standings, ["Round"], ["desc"]);
}

function columns() {
  return [
    {
      title: "Position",
      dataIndex: "value",
      render: (value: any, row: any, index: number) => {
        return <span className="PositionNumber">{index + 1}</span>;
      }
    },
    {
      title: "Team Name",
      dataIndex: "Name",
      render: (value: any) => {
        return <span className="PositionNumber">{value}</span>;
      }
    },
    {
      title: "Last Games",
      key: "LastFiveGamesOutcome",
      dataIndex: "LastFiveGamesOutcome",
      render: (tags: any) => (
        <>
          {tags.map((tag: GameOutcome) => {
            let color = "gold";
            if (tag.Value === "W") {
              color = "green";
            } else if (tag.Value === "L") {
              color = "red";
            }

            return (
              <Tag color={color} className="ScoreBox" key={tag.Key}>
                {tag.Value.toUpperCase()}
              </Tag>
            );
          })}
        </>
      )
    },
    {
      title: "Games Played",
      dataIndex: "GamesPlayed",
      key: "GamesPlayed"
    },
    {
      title: "Win",
      dataIndex: "GameWon",
      key: "GameWon"
    },
    {
      title: "Draw",
      dataIndex: "GameDraw",
      key: "GameDraw"
    },
    {
      title: "Lost",
      dataIndex: "GameLost",
      key: "GameLost"
    },
    {
      title: "Goals Scored",
      dataIndex: "GoalsScored",
      key: "GoalsScored"
    },
    {
      title: "Goals Received",
      dataIndex: "GoalsReceived",
      key: "GoalsReceived"
    },
    {
      title: "Goals Difference",
      dataIndex: "GoalsDiff",
      key: "GoalsDiff"
    },
    {
      title: "Points",
      dataIndex: "Points",
      key: "Points"
    }
  ];
}

export default Standings;
