import data from '../data/data.json';
import { ResultModel, TeamModel, GameOutcome } from '../models/resultModel';
import _ from 'lodash';

const DataService = {
    getRoundResults: () => {

        let results: ResultModel[] = [];
        data.forEach(round => {

            let roundTeams: TeamModel[] = [];

            round.matches.forEach((match: { [s: string]: any; } | ArrayLike<any>) => {
                const teams = Object.keys(match);
                const scores = Object.values(match);

                roundTeams.push({
                    Name: teams[0],
                    GameWon: scores[0] > scores[1] ? 1 : 0,
                    GameLost: scores[1] > scores[0] ? 1 : 0,
                    GameDraw: scores[1] === scores[0] ? 1 : 0,
                    Points: scores[0] > scores[1] ? 3 : scores[1] === scores[0] ? 1 : 0,
                    GoalsDiff: scores[0] - scores[1],
                    GamesPlayed: 1,
                    GoalsReceived: scores[1],
                    GoalsScored: scores[0],
                    LastFiveGamesOutcome: []
                });

                roundTeams.push({
                    Name: teams[1],
                    GameWon: scores[1] > scores[0] ? 1 : 0,
                    GameLost: scores[0] > scores[1] ? 1 : 0,
                    GameDraw: scores[1] === scores[0] ? 1 : 0,
                    Points: scores[1] > scores[0] ? 3 : scores[1] === scores[0] ? 1 : 0,
                    GoalsDiff: scores[1] - scores[0],
                    GamesPlayed: 1,
                    GoalsReceived: scores[0],
                    GoalsScored: scores[1],
                    LastFiveGamesOutcome: []
                });
            });

            results.push({ Round: round.round, Teams: roundTeams });
        });

        return results;
    },

    calculateTeamPositionsPerRound: function () {
        const tempRoundResults: ResultModel[] = this.getRoundResults();
        const sumRoundResults: ResultModel[] = [];

        for (let index = 0; index < _.sortBy(tempRoundResults, ['Round'], ['asc']).length; index++) {

            let rounds = _.filter(tempRoundResults, function (o) { return o.Round <= index + 1; });

            const output = _(rounds.flatMap(r => r.Teams))
                .groupBy('Name')
                .map((objs, key) => ({
                    'Name': key,
                    'GameWon': _.sumBy(objs, 'GameWon'),
                    'GameDraw': _.sumBy(objs, 'GameDraw'),
                    'GameLost': _.sumBy(objs, 'GameLost'),
                    'Points': _.sumBy(objs, 'Points'),
                    'GoalsDiff': _.sumBy(objs, 'GoalsDiff'),
                    'GamesPlayed': _.sumBy(objs, 'GamesPlayed'),
                    'GoalsReceived': _.sumBy(objs, 'GoalsReceived'),
                    'GoalsScored': _.sumBy(objs, 'GoalsScored'),
                    'LastFiveGamesOutcome': this.calculateLastFive(objs, index)
                }))
                .value();

            let teams: TeamModel[] = [];

            output.forEach(o => {
                teams.push({
                    Name: o.Name,
                    GameWon: o.GameWon,
                    GameDraw: o.GameDraw,
                    GameLost: o.GameLost,
                    Points: o.Points,
                    GoalsDiff: o.GoalsDiff,
                    GamesPlayed: o.GamesPlayed,
                    GoalsReceived: o.GoalsReceived,
                    GoalsScored: o.GoalsScored,
                    LastFiveGamesOutcome: o.LastFiveGamesOutcome
                });
            });

            sumRoundResults.push({
                Round: tempRoundResults[index].Round,
                Teams: _.orderBy(teams, ['Points', 'GoalsDiff', 'GoalsScored'], ['desc', 'desc', 'desc'])
            });
        }

        return sumRoundResults;
    },

    calculateLastFive: function (teams: TeamModel[], counter: number) {

        const range = teams.length <= 5 ? 0 : teams.length - 5;

        const lastFive: GameOutcome[] = [];

        for (let index = range; index < teams.length; index++) {
            const team = teams[index];

            lastFive.push(this.getGameOutcome(team, index))
        }

        return _.reverse(lastFive);

    },

    getGameOutcome: (team: TeamModel, index: number) => {

        const outcome: GameOutcome = { Key: index, Value: '' };

        if (team.GameWon === 1) {
            outcome.Value = 'W'
            return outcome;
        }

        if (team.GameLost === 1) {
            outcome.Value = 'L'
        }

        if (team.GameDraw === 1) {
            outcome.Value = 'D'
        }

        return outcome;
    }
};

export default DataService;
